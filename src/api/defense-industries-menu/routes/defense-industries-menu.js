'use strict';

/**
 * defense-industries-menu router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::defense-industries-menu.defense-industries-menu');
