'use strict';

/**
 * defense-industries-menu service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::defense-industries-menu.defense-industries-menu');
