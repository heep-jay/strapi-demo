'use strict';

/**
 * defense-industries-menu controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::defense-industries-menu.defense-industries-menu');
