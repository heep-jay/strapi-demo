'use strict';

/**
 * halogen-thought-leadership-post controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::halogen-thought-leadership-post.halogen-thought-leadership-post');
