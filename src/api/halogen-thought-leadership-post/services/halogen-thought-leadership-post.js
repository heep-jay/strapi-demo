'use strict';

/**
 * halogen-thought-leadership-post service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::halogen-thought-leadership-post.halogen-thought-leadership-post');
