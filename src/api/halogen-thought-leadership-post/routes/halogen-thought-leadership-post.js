'use strict';

/**
 * halogen-thought-leadership-post router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::halogen-thought-leadership-post.halogen-thought-leadership-post');
