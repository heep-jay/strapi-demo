'use strict';

/**
 * halosphere-client-facing router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::halosphere-client-facing.halosphere-client-facing');
