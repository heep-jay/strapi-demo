'use strict';

/**
 * halosphere-client-facing controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::halosphere-client-facing.halosphere-client-facing');
