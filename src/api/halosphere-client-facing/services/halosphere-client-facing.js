'use strict';

/**
 * halosphere-client-facing service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::halosphere-client-facing.halosphere-client-facing');
