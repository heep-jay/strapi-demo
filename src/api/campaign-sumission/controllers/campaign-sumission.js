'use strict';

/**
 * campaign-sumission controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::campaign-sumission.campaign-sumission');
