'use strict';

/**
 * campaign-sumission router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::campaign-sumission.campaign-sumission');
