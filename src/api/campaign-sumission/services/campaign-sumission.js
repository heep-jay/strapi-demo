'use strict';

/**
 * campaign-sumission service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::campaign-sumission.campaign-sumission');
