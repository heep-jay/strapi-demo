'use strict';

/**
 * security-report router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::security-report.security-report');
