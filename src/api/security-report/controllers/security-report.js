'use strict';

/**
 * security-report controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::security-report.security-report');
