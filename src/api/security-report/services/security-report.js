'use strict';

/**
 * security-report service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::security-report.security-report');
