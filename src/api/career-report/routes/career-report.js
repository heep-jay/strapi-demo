'use strict';

/**
 * career-report router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::career-report.career-report');
