'use strict';

/**
 * career-report controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::career-report.career-report');
