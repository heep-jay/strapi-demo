'use strict';

/**
 * career-report service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::career-report.career-report');
